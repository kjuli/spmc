def do():
    entities = ['theorem', 'definition' , 'assumption', 'proposition', 'lemma', 'corollary', 'claim', 'remark', 'example']

    for entity in entities:
        print(f"\\crefname{{{entity}}}{{{entity}}}{{{entity}s}}")
        print(f"\\Crefname{{{entity}}}{{{entity.capitalize()}}}{{{entity.capitalize()}s}}")

if __name__ == '__main__':
    do()