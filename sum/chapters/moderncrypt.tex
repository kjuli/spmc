\chapter{Introduction to Modern Cryptography}
\section{Symmetric Encryption One: One-Time Pads and Blocks}
\begin{assumption}[Kerkhoff's principle]
    The attacker knows \textbf{all details} of the cryptographic scheme. The security of the system should \underline{not} depend on whether the algorithm of the encryption scheme is secret, but only whether the actual key is secret. No ``security by obscurity''.
\end{assumption}
\subsection{One-Time symmetric encryption \& Cryptosystems}
\begin{definition}[Cryptosystem]\label{def:1:1:cryptosystem}
    A \textbf{cryptosystem} is a tuple
    \begin{align*}
        S = (X,K,Y,e,d)
    \end{align*}
    with
    \begin{itemize}
        \item $X \neq \emptyset$: Set of \textit{plaintexts}
        \item $K \neq \emptyset, |K| < \infty$: Set of \textit{keys}
        \item $Y$: Set of \textit{ciphertexts}
        \item $e: X \times K \to Y$: The encryption function
        \item $d: Y \times K \to X$: The decryption function
    \end{itemize}
    It holds \textbf{Perfect Correctness}: $\forall x \in X\ \forall k \in K: d(e(x,k), k) = x$. Furthermore, there are no \textbf{unnecessary ciphertexts}: $Y = \dset{e(x,k)}{x \in X, k \in K}$.
\end{definition}
\begin{definition}[Cipher]\label{def:1:1:cipher}
    For a fixed $k \in K$, we call the function $e(\cdot,k): X \to Y: x \mapsto e(x,k)$ a \textbf{cipher}.
\end{definition}
\begin{definition}[Vernam System / One-Time Pad]\label{def:1:1:vernam}
    Let $\ell > 0$. The \textbf{Vernam (crypto) system} (or \textbf{one-time pad}) of length $\ell$ is the cryptosystem $(M,M,M,e,d)$ where
    \begin{enumerate}
        \item $M = \set{0,1}^\ell$
        \item $e(x,k) = x \oplus k$
        \item $d(y,k) = y \oplus k$
    \end{enumerate}
\end{definition}
\begin{proposition}[Vernam System is a cs]\label{prop:1:1:vernam:cs}
    The Vernam system (\ref{def:1:1:vernam}) is a cryptosystem (\ref{def:1:1:cryptosystem}).
\end{proposition}
\begin{proof}
    We have to prove \textbf{perfect correctness} and \textbf{no unncessary ciphertexts}
    \begin{enumerate}
        \item It holds true that $(x \oplus k) \oplus x = x$.
        \item $Y = M = \set{0,1}^\ell = \dset{x \oplus 0^\ell}{x \in \set{0,1}^\ell} \subseteq \dset{e(x,k)}{x,k \in \set{0,1}^\ell}$.\\
        Obviously, $Y \supseteq \dset{e(x,k)}{x,k \in \set{0,1}^\ell}$
    \end{enumerate}
    {\ }
\end{proof}
\subsection{Perfect Secrecy}
\begin{definition}[Cryptosystems with Key Dist.]\label{def:1:1:crypt}
    We call a typle $\mcV = (X,K,Y,e,d,P_K)$ \textbf{cryptosystem with key distribution} if
    \begin{enumerate}
        \item $(X,K,Y,e,d)$ is a cryptosystem
        \item $P_K$ is a key distribution over $K$
    \end{enumerate}
    We write $S[P_K] = \mcV$ to combine a cryptosystem $S$ with key dist. $P_K$.
\end{definition}
\begin{definition}[Prob. Dist. over $X \times K \times Y$]\label{def:1:1:probdistinduced}
    The probability distribution $P$ over $X \times K \times Y$ induced by $P_K$ and $P_X$ is defined as
    \begin{align*}
        P(x,k,y) = \begin{cases}
            P_X(x) \cdot P_Y(y) & \text{if } e(x,k) = y \\
            0 & \text{else}
        \end{cases}
    \end{align*}
\end{definition}
\begin{lemma}[Prob. is indeed dist.]\label{lem:1:1:probdistinduced}
    Let $P$ be the induced probability distribution over $X \times K \times Y$ by $P_K$ and $P_X$ as in \Cref{def:1:1:probdistinduced}. Then it holds true that $P$ is a probability distribution over $X \times K \times Y$.
\end{lemma}
\begin{proof}
    First, $P(x,k,y) \geq 0$ is obviously fulfilled. We need to show that $\sum_\omega P(\omega) = 1$ for $\omega = (x,k,y) \in X \times K \times Y$.
    \begin{align*}
        \sum_{(x,k,y) \in X \times K \times Y} P(x,k,y) &= \sum_{x\in X,k \in K} P(x,k,e(x,k)) \\
        &= \sum_{x\in X, k \in K} P_X(x) \cdot P_K(k) \\
        &= \underbrace{\sum_{x \in X} P_X(x)}_{=1} \cdot \underbrace{\sum_{k \in K} P_K(k)}_{=1} \\
        &= 1
    \end{align*}
\end{proof}
\begin{lemma}[$P(y)$]\label{lem:1:1:py}
    It holds true that
    \begin{align*}
        P(y) = \sum_{\substack{x \in X, k \in K \\ e(x,k) = y}} P(x) \cdot P(k)
    \end{align*}
    Note that $P(x) = P_X(x)$ and $P(k) = P_K(k)$ (both lemmas)
\end{lemma}
\begin{proof}
    \begin{align*}
        P(y) = P(E_y) &= P(\dset{(x,k,y)}{x \in X, k \in K}) = P\left(\bigcup_{x\in X, k \in K} \set{(x,k,y)}\right) \\
        &= \sum_{x \in X, k \in K} P(x,k,y) = \sum_{\substack{x \in X,k \in K \\ e(x,k) = y}} P_X(x) \cdot P_K(k) \\
        &= \sum_{\substack{x \in X,k \in K \\ e(x,k) = y}} P(x) \cdot P(k) 
    \end{align*}
\end{proof}
\begin{definition}[Perfect Secrecy]\label{def:1:1:perfectsecrecy}
    A cryptosystem with key distribution $\mcV$ (\ref{def:1:1:crypt}) provides \textbf{perfect secrecy} if $\forall P_X$ plaintext distribution the following holds true:
    \begin{align*}
        P(x) = P(x | y)\ \forall x \in X\ \forall y \in Y, P(y) > 0
    \end{align*}
\end{definition}
\begin{theorem}[Perfect Secrecy Re-Defined]\label{thm:1:1:perfectsecrecy}
    Let $\mcV$ be a cryptosystem with key distribution (\ref{def:1:1:crypt}), then the statements are equivalent:
    \begin{enumerate}
        \item $\mcV$ provides perfect secrecy (\ref{def:1:1:perfectsecrecy})
        \item $\forall x,x^\prime \in X\ \forall y \in Y: P_K(\dset{k}{k \in K, e(x,k) = y}) = P_K(\dset{k}{k \in K, e(x^\prime,k) = y})$
    \end{enumerate}
\end{theorem}
\begin{proof} We only show 2. $\implies$ 1.\\
    ``$\impliedby$'': We have to show that $\forall P_X, x \in X, y \in Y, P(y) > 0: P(x) = P(x|y)$.
    \begin{description}
        \item[Case $P(x) = 0$] Then $0 \leq P(x|y) = \frac{P(x,y)}{P(y)} \leq \frac{P(x)}{P(y)} = \frac{0}{P(y)} = 0 = P(x)$, since $E_{x,y} \subseteq E_y$
        \item[Case $P(x) > 0$] Then
        \begin{align*}
            P(x|y) = \dfrac{P(y|x)\cdot P(x)}{P(y)} &= \dfrac{P(y|x)\cdot P(x)}{\sum_{x^\prime \in X:\ P(x^\prime) > 0} P(y|x^\prime) \cdot P(x^\prime)} \\
            &\overset{\text{\ref{lem:1:1:pyxiskeydist}}}{=} \dfrac{P(y|x)P(x)}{\sum_{x^\prime} P_K(\dset{k}{k \in K, e(x^\prime,k) = y})P(x^\prime)} \\
            &=\dfrac{P(y|x)P(x)}{\sum_{x^\prime} P_K(\dset{k}{k \in K, e(x,k) = y})P(x^\prime)} \\
            &\overset{\text{\ref{lem:1:1:pyxiskeydist}}}{=} \dfrac{P(y|x)P(x)}{P(y|x)\underbrace{\sum_{x^\prime} P(x^\prime)}_{=1}} \\
            &= P(x)
        \end{align*}  
    \end{description}
\end{proof}
\begin{lemma}[$P(y|x)$ is key dist.]\label{lem:1:1:pyxiskeydist}
    Let $\mcV$ be cryptosystem with key distribution (\ref{def:1:1:crypt}), $P_X$ a plaintext distribution and $P$ the induced probability by $P_X$ and $P_K$ (\ref{def:1:1:probdistinduced}). Then $\forall x \in X, y \in Y$ such that $P_X(x) > 0$ it holds true that $P_K(\dset{k}{k \in K, e(x,k) = y}) = P(y | x)$.
\end{lemma}
\begin{proof}
    Let $x \in X, y \in Y$ and $P(x) > 0$.
    \begin{align*}
        P(y|x) &= \dfrac{P(y,x)}{P(x)} = \dfrac{P(\dset{(x,k,y)}{k \in K})}{P(x)} \\
        &= \dfrac{\sum_k P(x,k,y)}{P(x)} = \dfrac{\sum_{k \in K, e(x,k) = y} P_X(x) P_K(k)}{P(x)} \\
        &\overset{P_X(x) = P(x)}{=} \sum_{k \in K, e(x,k) = y} P_K(k) \\
        &= P_K(\dset{k}{k \in K, e(x,k) = y})
    \end{align*}
\end{proof}
\begin{lemma}[Vernam provides PS]\label{lem:1:1:vernamps}
    Let $\ell > 0$. The Vernam system (\ref{def:1:1:vernam}) of length $\ell$ with the uniform key distribution $P_K$ provides \textbf{perfect secrecy} (\ref{def:1:1:perfectsecrecy}).
\end{lemma}
\begin{proof}
    By \Cref{thm:1:1:perfectsecrecy}, we just need to show that $\forall x,x^\prime,y \in \set{0,1}^\ell:\ P_K(\dset{k \in K}{e(x,k) = y}) = P_K(\dset{k \in K}{e(x^\prime, k) = y})$.
    Observe that there is exactly one $\hat{k}$ such that $e(x,\hat{k}) = y$, thus $P_K(\dset{k \in K}{e(x,k) = y}) = P\left(\set{\hat{k}}\right) = \frac{1}{|K|}$. By the same argument, it follows that $P_K(\dset{k \in K}{e(x^\prime,k) = y}) = \frac{1}{|K|}$.
\end{proof}
\begin{theorem}[Cardinality of Plaintext and Key Set]\label{thm:1:1:plainkeycar}
    Let $\mcS = (X,K,Y,e,d)$ be a cryptosystem (\ref{def:1:1:cryptosystem}) providing perfect secrecy (\ref{def:1:1:perfectsecrecy}), then $|K| \geq |X|$ (and even $|K| \geq |Y| \geq |X|$)
\end{theorem}
\begin{proof}
    Assume by contradiction that $|K| < |X|$. Let $P_X$ be the uniform distribution and $y \in Y$ a ciphertext with $P(y) > 0$. Define $X_y = \dset{x \in X}{\exists k \in K:\ d(y,k) = x}$ to be the set of plaintexts that are possible decryptions of $y$. Then we know thath $|X_y| \leq |K| < |X|$ as each key decrypts $y$ to exactly one plaintext. Thus, there is at least one plaintext $\tilde{x} \in X \setminus X_y$ that is not a possible decryption of $y$. Hence, $P(\tilde{x} | y) = 0 \neq \frac{1}{|X|} = P(\tilde{x})$, thus $\mcS$ is not perfectly secret, but this contradicts the assumption.
\end{proof}
\begin{theorem}[Shannon]\label{thm:1:1:shannon}
    Let $\mcV = \mcS[P_K]$ be a cryptosystem with key distribution $P_K$ (\ref{def:1:1:crypt}) and $|X| = |Y| = |K|$. Then the following statements are equivalent:
    \begin{enumerate}
        \item $\mcV$ provides perfect secrecy (\ref{def:1:1:perfectsecrecy})
        \item $P_K$ is uniform distribution and $\forall x \in X, y \in Y\ \exists k \in K:\ e(x,k) = y$.
    \end{enumerate}
\end{theorem}
\begin{corollary}[Vernam is perfect s.]\label{cor:1:1:vernamps}
    From $\Cref{thm:1:1:shannon}$ it follows directly that Vernam (\ref{def:1:1:vernam}) has perfect secrecy (\ref{def:1:1:perfectsecrecy}).
\end{corollary}

\subsection{Multiple Messages with Constant Length and No Rep.}
\begin{definition}[Substitution System]\label{def:1:1:substitutionsystem}
    Let $X \neq \emptyset$ be a finite set. A \textbf{substitution cryptosystem} over $X$ is a tuple $(X, \mcP_X, X, e, d)$ where $\mcP_X$ is the set of all permutations of $X$, i.e. $\mcP_X = \dset{\pi: X \to X}{\pi \text{ is bijective}}$, and $e(x,\pi) = \pi(x)$, $d(y,\pi) = \pi^{-1}(y)$ for all $x,y \in X$ and $\pi \in \mcP_x$.
\end{definition}
\begin{lemma}[SubSys is PS]\label{lem:1:1:subsysps}
    The substitution cryptosystem (\ref{def:1:1:substitutionsystem}) provides perfect security\footnote{Just an extension of perfect secrecy (\ref{def:1:1:perfectsecrecy}) for this scenario}. Furthermore, it is \textbf{optimal}, i.e. every other secure cryptosystem is just a variation of it.
\end{lemma}
\begin{definition}[$\ell$-Block Cipher]\label{def:1:1:ell-blockcipher}
    Let $\ell: \bbN \to \bbN$ be a polynomial. An \textbf{$\ell$-block cipher} $\mcB$ is a cryptosystem of the form $(\set{M_\eta}_{\eta\in\bbN},\Gen(1^\eta),\set{M_\eta}_{\eta\in\bbN}, E, D)$, where $M_\eta = \set{0,1}^{\ell(\eta)}$, with
    \begin{itemize}
        \item \textbf{Security Parameter} $\eta\in\bbN$
        \item Probabilistic Key Generation Algorithm $\Gen(1^\eta)$
        \item Deterministic Encryption Algorithm $E(x,k)$
        \item Deterministic Decryption Algorithm $D(y,k)$
    \end{itemize}
\end{definition}

\section{Block-Ciphers}
\subsection{Substitution-Permutation Cryptosystems}
\begin{definition}[Substitution-Permutation Network (SPN)]\label{def:1:2:spn}
    A \textbf{substitution-permutation network (SPN)} is a tuple $\mcN = (m,n,r,s,S,\beta,K)$ where
    \begin{itemize}
        \item $m \in \bbN$: Number of words
        \item $n \in \bbN$: Word length
        \item $r \in \bbN$: Number of rounds
        \item $s \in \bbN$: Key length
        \item $S \in \mcP_{\set{0,1}^n}$: \textbf{S-Box}
        \item $\beta\in \mcP_{[m\cdot n]}$: Self-inverse bit permutation (i.e. $\beta = \beta^{-1}$)
        \item $K: \set{0,1}^s \times [r + 1] \to \set{0,1}^{mn}$: \textbf{Round Key Function}.
    \end{itemize}
\end{definition}
\begin{definition}[Substitution-Permutation Cryptosystem (SPCS)]\label{def:1:2:spcs}
    The \textbf{substitution-permutation cryptosystem (SPCS)} $\mcB(\mcN)$ induced by $\mcN$ (\ref{def:1:2:spn}) is the $mn$-block cipher (\ref{def:1:1:ell-blockcipher}) $\mcB(\mcN) = (M,\Gen(1^s),M,E,D)$ where $M = \set{0,1}^{mn}$, and $E$ is \Cref{alg:1:2:encryptionforspcs}.
\end{definition}
\begin{algorithm}[h!]
    \caption{Encryption Algorithm for SPCS}
    \hspace*{\algorithmicindent} \textbf{Output} $\set{0,1}^{mn}$
    \begin{algorithmic}[1]
        \Function{$E$}{$x \in \set{0,1}^{mn}, k \in \set{0,1}^s$}
            \State $u = x \oplus K(k,0)$ \Comment{initial white step (round key add.)}
            \For{$i = 1,...,r-1$}
                \For{$j = 0,...,m-1$} \Comment{Word Substition}
                    \State $v^{(j)} = S(u^{(j)})$
                \EndFor
                \State $w = v^\beta$ \Comment{Bit Permutation}
                \State $u = w \oplus K(k,i)$ \Comment{Round Key Addition}
            \EndFor
            \For{$j = 0,...,m-1$}
                $v^{(j)} = S(u^{(j)})$
            \EndFor
            \State $y = v \oplus K(k,r)$
            \State\Return $y$
        \EndFunction
    \end{algorithmic}
    \label{alg:1:2:encryptionforspcs}
\end{algorithm}
\subsubsection*{Linear Cryptanalysis}
Linear Cryptanalysis is an attack based on known plain-texts trying to attack a block-cipher (\ref{def:1:1:ell-blockcipher}).
\begin{description}
    \item[Pre-Ciphertext $z$] Intermediate result before last round, i.e. $u_{r-1}$.
    \item[Final Round Key $K(k,r)$] Round key of the last round.
    \item[Map $E_0(x,k)$] Maps the plaintext $x$ and key $k$ to the pre-ciphertext.
    \begin{align}
        y = E(x,k) \implies \Big(\forall t < m:\ y^{(t)} = S(E_0(x,k)^{(t)}) \oplus K(k,r)^{(t)} \\
        \implies \forall t < m:\ E_0(x,k)^{(t)} = S^{-1}(y^{(t)} \oplus K(k,r)^{(t)})\Big)
    \end{align}
    \item[Pre-Ciphertext Word $E_0(x,k)^{(t)}$] Here, $t$ is fixed.
    \item[Final Round Key $K(k,r)^{(t)}$] Final word. Determine $K(k,r)^{(t)}$. 
\end{description}
\begin{algorithm}[h!]
    \caption{Search Algorithm for final round}
    \hspace*{\algorithmicindent} Let $T$ be a set of plaintext/ciphertext pairs encrypted under the same (unknown) key $\overline{k}$.\\
    \hspace*{\algorithmicindent} \textbf{Output:} Candidate set
    \begin{algorithmic}[1]
        \Function{$\mathtt{SearchFinalRoundKeyWord}$}{$T \subseteq \dset{(x,y) \in X \times Y}{y = e(x,\overline{k})}$}
            \State $\mathrm{CandidateSet} = \emptyset$ \Comment{Initialize set of key candidates}
            \For{$\kappa\in\set{0,1}^n$} \Comment{Go through all final round key words.}
                \State $U = \dset{(x, S^{-1}(y^{(t)} \oplus \kappa))}{(x,y) \in T}$ \Comment{Determine hypothetical plain/cipher word pairs}
                \If{$\mathtt{Test}(U)$} \Comment{Test hypothetical set $U$}
                    \State $\mathrm{CandidateSet} = \mathrm{CandidateSet} \cup \set\kappa$
                \EndIf
            \EndFor
            \State $\mathrm{CanidateSet}$
        \EndFunction
    \end{algorithmic}
    \label{alg:1:2:search}
\end{algorithm}
In \Cref{alg:1:2:search}, $\mathtt{Test}(U) :\iff \exists k:\ z = E_0(x,k)^{(t)} \forall (x,z) \in U$.
\begin{algorithm}[h!]
    \caption{Linear approximation test algorithm implementing $\mathtt{Test}(U)$}
    \hspace*{\algorithmicindent} \textbf{Output:} Candidate set
    \begin{algorithmic}[1]
        \Function{$\mathtt{LinearApproximationtest}$}{$U \subseteq \set{0,1}^{mn} \times \set{0,1}^n$}
            \State $c = 0$ \Comment{Init. of counter for number of pairs fulfilling the condition}
            \For{$(x,z) \in U$} \Comment{Count how many plain/pre-cipher words are ful. the eq.}
                \If{$x(i_0) \oplus ... \oplus x(i_{p-1}) = z(j_0) \oplus ... \oplus z(j_{p-1})$}
                    \State $c := c + 1$
                \EndIf
            \EndFor
            \State \Return $2 \cdot \left|\frac{c}{|U|} - \frac{1}{2}\right| \geq \delta$ \Comment{Check if percentage of ful. pairs is above threshhold}
        \EndFunction
    \end{algorithmic}
\end{algorithm}

\begin{eq}[Equation $G_I^J$]\label{eq:1:2:gij}
    Let $a,b \in \bbN$, $x \in \set{0,1}^a$, $y \in \set{0,1}^b$, $I \subseteq [a]$, $J \subseteq [b]$. We denote the equation
    \begin{align*}
        \bigoplus_{i \in I} x(i) = \bigoplus_{j\in J} y(j)
    \end{align*}
    by $G_I^J(x,y)$
\end{eq}
\begin{eq}[Equation $n_I^J$]\label{eq:1:2:nij}
    For a function $f: \set{0,1}^a \to \set{0,1}^b$, $n_I^J[f]$ counts how many times the equation $G_I^J(x,f(x))$ is \textit{false}, i.e.
    \begin{align*}
        n_I^J[f] = \sum_{x \in \set{0,1}^a} \left(\bigoplus_{i \in I} x(i) \oplus \bigoplus_{j \in J} f(x)(j)\right)
    \end{align*}
\end{eq}
\begin{definition}[Orientation]\label{def:1:2:orientation}
    For $J,I,f$ as in \Cref{eq:1:2:gij,eq:1:2:nij} we define
    \begin{align*}
        \epsilon^J_I[f] = 1 - 2 \dfrac{n_I^J[f]}{2^a} \in [-1,1]
    \end{align*}
    as the \textbf{orientation} of $f$ regarding $I$ and $J$.
\end{definition}
We have
\begin{itemize}
    \item $\epsilon_I^J[f] = 1 \iff G_I^J(x,f(x))$ is true for all $x \in \set{0,1}^a$
    \item $\epsilon_I^J[f] = -1 \iff G_I^J(x,f(x))$ is false for all $x \in \set{0,1}^a$
\end{itemize}
\begin{definition}[Linear Dependence]\label{def:1:2:lineardependence}
    Let $I \subseteq [a], J \subseteq [b], a,b \in \bbN$. We call the pairs of sets $(I,J)$ a \textbf{linear dependence} for $f$ and a \textit{threshold value} $\delta$ if $|\epsilon^I_J[f]| \geq \delta$ (\ref{def:1:2:orientation}).
\end{definition}
\begin{definition}[Linear Dependence for SPCS]\label{def:1:2:ldforspcs}
    Let $(I,J)$ be a pair of sets with $I \subseteq [mn]$ ($mn$: length of plaintext, $m$: number of words) and $\emptyset \neq J \subseteq [n]$ ($n$: length of a word). If $(I,J)$ is a linear dependence for $\delta$ and $E_0^t(\cdot, k)$ for \textbf{every} key $k$, then we call $(I,J)$ a \textbf{linear dependence for an SPCS} wrt. a threshold value $\delta$ and the $t$-th final round key word.
\end{definition}
\begin{lemma}[Auxiliary Lemma, Probability Space]\label{lem:1:2:aux}
    Let $(I,J)$ with $I \subseteq [mn], \emptyset \neq J \subseteq [n]$ and the random variable $X_I^J[f]: \set{0,1}^a \to \set{-1,1}$ defined by
    \begin{align*}
        X_I^J[f](x) = 1 - 2 \left(\bigoplus_{i \in I} x(i) \oplus \bigoplus_{j \in J}f(x)(j)\right)
    \end{align*}
    The underlying probability space is $\set{0,1}^a$ with the uniform distribution. Then
    \begin{align*}
        \epsilon_I^J[f] = \sum_{x \in \set{0,1}^a} X_I^J[f](x) \cdot 2^{-a} =: Exp(X_I^J[f])
    \end{align*}
\end{lemma}
\begin{proof}
    \begin{align*}
        \epsilon^J_I[f] = 1- 2\frac{n_I^J[f]}{2^a} = 1 - \frac{2}{2^a} \sum_{x \in \set{0,1}^a} \left(\bigoplus_{i \in I} x(i) \oplus \bigoplus_{j \in J}f(x)(j)\right) = \\
        = \sum_{x \in \set{0,1}^a} \left(1 - 2\bigoplus_{i \in I}x(i) \oplus \bigoplus_{j \in J}f(x)(j)\right)2^{-a} = \sum_{x \in \set{0,1}^a} X_I^J[f](x)2^{-a} = Exp(X^J_I[f])
    \end{align*}
\end{proof}
\begin{lemma}[Parallel Composition]\label{lem:1:2:parcom}
    Let $a,a^\prime,b,b^\prime\in\bbN$, $f: \set{0,1}^a \to \set{0,1}^b$, $f^\prime: \set{0,1}^{a^\prime} \to \set{0,1}^{b^\prime}$ be two functions and $h: \set{0,1}^{a + a^\prime} \to \set{0,1}^{b + b^\prime}$ defined by $h(xx^\prime) = f(x)f^\prime(x^\prime)$ for $x \in \set{0,1}^a$, $x^\prime\set{0,1}^{a^\prime}$. Furthermore, $I \subseteq [a]$, $I^\prime \subseteq [a^\prime]$, $J \subseteq [b]$, $J^\prime \subseteq [b^\prime]$. For $L = I \cup (a + I^\prime)$ and $M = J \cup (b + J^\prime)$ it holds true that
    \begin{align*}
        \epsilon_L^M[h] = \epsilon_I^J[f] \cdot \epsilon^{J^\prime}_{I^\prime}[f^\prime]
    \end{align*}
\end{lemma}
\begin{proof}
    For $H: \set{0,1} \to \set{-1,1}$ with $H(x) = 1 - 2x$ it holds true that $H(x \oplus x^\prime) = H(x) \cdot H(x^\prime)$ for $x,x^\prime \in \set{0,1}$. Furthermore
    \begin{align*}
        \epsilon_L^M[h] &= 2^{-(a + a^\prime)} \sum_{\substack{x \in \set{0,1}^a \\ x^\prime \in \set{0,1}^{a^\prime}}} \left(1 - 2 \left(\bigoplus_{i \in L} (xx^\prime)(i) \oplus \bigoplus_{j \in M}h(xx^\prime)(j)\right)\right) \\
        &= 2^{-(a + a^\prime)} \sum_{\substack{x \in \set{0,1}^a \\ x^\prime \in \set{0,1}^{a^\prime}}} \left(1 - 2 \left(\bigoplus_{i \in I} x(i) \oplus \bigoplus_{i^\prime \in I^\prime} x^\prime(i^\prime) \oplus \bigoplus_{j \in J}f(x)(j) \oplus \bigoplus_{j^\prime \in J^\prime} f^\prime(x^\prime)(j^\prime)\right)\right) \\
        &= 2^{-(a + a^\prime)} \sum_{\substack{x \in \set{0,1}^a \\ x^\prime \in \set{0,1}^{a^\prime}}} \left(1 - 2 \left(\bigoplus_{i \in I} x(i) \oplus \bigoplus_{j \in J}f(x)(j) \oplus \bigoplus_{i^\prime \in I^\prime} x^\prime(i^\prime) \oplus \bigoplus_{j^\prime \in J^\prime} f^\prime(x^\prime)(j^\prime)\right)\right)  \\
        &= 2^{-(a + a^\prime)} \sum_{\substack{x \in \set{0,1}^a \\ x^\prime \in \set{0,1}^{a^\prime}}} \left[\left(1 - 2\left(\bigoplus_{i \in I} x(i) \oplus \bigoplus_{j \in J}f(x)(j)\right)\right) \cdot \left(1 - 2\left(\bigoplus_{i^\prime \in I^\prime}x^\prime(i^\prime) \oplus \bigoplus_{j^\prime \in J^\prime} f^\prime(x^\prime)(j^\prime)\right)\right)\right]\\
        &= 2^{-(a + a^\prime)} \sum_{x,x^\prime} X^J_I[f](x) \cdot X^{J^\prime}_{I^\prime}[f^\prime](x^\prime) \\
        &= 2^{-a} \sum_x X^J_I[f](x) \cdot 2^{-a}\sum_{x^\prime} X^{J^\prime}_{I^\prime}[f^\prime](x^\prime) \\
        &= Exp(X^J_I[f]) \cdot Exp(X^{J^\prime}_{I^\prime}[f^\prime]) \\
        &= \epsilon_I^J[f] \cdot \epsilon_{I^\prime}^{J^\prime}[f^\prime]
    \end{align*}
\end{proof}
\begin{lemma}[Bit Permutation]\label{lem:1:2:bitperm}
    Let $f: \set{0,1}^a \to \set{0,1}^b$ be a function, $I \subseteq [a], J \subseteq [b]$ and $\beta \in \mcP_{[b]}$. The function $h: \set{0,1}^a \to \set{0,1}^b$ is defined by $h(x) = f(x)^\beta$ for $x \in \set{0,1}^a$. For $J^\prime = \dset{\beta(j)}{j \in J}$ it holds true that
    \begin{align*}
        \epsilon^{J^\prime}_I[h] = \epsilon^J_I[f]
    \end{align*}
\end{lemma}
\begin{lemma}[Key Addition]\label{lem:1:2:keyadd}
    Let $f: \set{0,1}^a \to \set{0,1}^b$ a function, $I \subseteq [a], J \subseteq [b]$ and $k \in \set{0,1}^b$. Furthermore, let $h: \set{0,1}^a \to \set{0,1}^b$ defined by $h(x) = f(x) \oplus k$ for $x \in \set{0,1}^a$. Then it holds true that
    \begin{align*}
        \epsilon_I^H[h] = \begin{cases}
            \epsilon_I^J[f] & \text{if } \bigoplus_{j \in J} k(j) = 0 \\
            -\epsilon_I^J[f] & \text{otherwise}
        \end{cases}
    \end{align*}
    For $g(x) = f(x \oplus k)$ for a $k \in \set{0,1}^a$ a similar statement holds true.
\end{lemma}
\begin{proof}
    Let $\bigoplus_{j \in J}k(j) = 1$. Then
    \begin{align*}
        \epsilon_I^J[h] = Exp(X^J_I[h]) = Exp(X^J_I[f \oplus k]) = Exp(-X^J_I[f]) = -Exp(X^J_I[f]) = -\epsilon^J_I[f]
    \end{align*}
    Analogously for $\bigoplus_j k(j) = 0$.
\end{proof}
\begin{lemma}[Sequential Composition]\label{lem:1:2:seqcomp}
    Let $f: \set{0,1}^a \to \set{0,1}^b$, $g: \set{0,1}^b \to \set{0,1}^c$ functions, $(I,J), (J,L)$ ideal ($\delta = 1$) linear dependencies (\ref{def:1:2:lineardependence}). Then $(I,L)$ is an ideal linear dependence for $g \circ f$ and it holds true that
    \begin{align*}
        \epsilon_I^L[g \circ f] = \epsilon_I^J[f] \cdot \epsilon_J^L[g]
    \end{align*}
\end{lemma}
\begin{proof}
    There exists $d,d^\prime \in \set{0,1}$ with $\forall x \in \set{0,1}^a: \bigoplus_i x(i) \oplus \bigoplus_j f(x)(j) = d$ and $\forall y \in \set{0,1}^b: \bigoplus_j y(j) \oplus \bigoplus_l g(y)(l) = d^\prime$. Then $\bigoplus_j f(x)(j) \oplus \bigoplus_l g(f(x))(l) = d^\prime$ for all $x$ and thus $\bigoplus_i x(j) \oplus \bigoplus_l g(f(x))(l) = d \oplus d^\prime$. Thus we conclude that $(I,L)$ is an ideal linear dependence for $g \circ f$.
\end{proof}
\subsection{Algorithmic Security on Block-Ciphers}
\begin{definition}[$\ell$-Distinguisher]\label{def:1:2:ldistinguisher}
    Let $\ell: \bbN \to \bbN$ be a polynomial and $\eta \in \bbN$. An $\ell$-distinguisher is a \textit{ppt algorithm} of the form $U(1^\eta, F): \set{0,1}$ where $F: \set{0,1}^{\ell(\eta)} \to \set{0,1}^{\ell(\eta)}$ is an oracle, either being a cipher from a block cipher or a cipher from a substitution cryptosystem.
\end{definition}
\begin{definition}[Security Game for Block-Ciphers $\bbE_U^\mcB$]\label{def:1:2:securitygame}
    Let $\ell: \bbN \to \bbN$ be a polynomial, $U$ be an $\ell$-distinguisher (\ref{def:1:2:ldistinguisher}) and $\mcB = (M_\eta, \Gen(1^\eta), M_\eta, E,D)$ with $M_\eta = \set{0,1}^{\ell(\eta)}$ be an $\ell$-block cipher (\ref{def:1:1:ell-blockcipher}) with key space $K$ derived from $\Gen(1^\eta)$. The \textbf{security game} for block ciphers $\bbE^\mcB_U$ is \Cref{alg:1:2:securitygame}. The shortened game $\bbS_U^\mcB$ directly returns $b^\prime$ instead of making a comparison.
\end{definition}
\begin{algorithm}[h!]
    \caption{Security Game for Block Cipher}
    \hspace*{\algorithmicindent} \textbf{Output:} $\set{0,1}$
    \begin{algorithmic}[1]
        \Function{$\bbE$}{$1^\eta$}
            \State $b \fgets \set{0,1}$ \Comment{Choose real world or random world}
            \If{$b = 1$}
                \State $k \fgets \Gen(1^\eta)$
                \State $F = E(\cdot,k)$
            \Else
                \State $F \fgets \mcP_{\set{0,1}^{\ell(\eta)}}$
            \EndIf
            \State $b^\prime \fgets U(1^\eta,F)$ \Comment{Guess phase}
            \State \Return $b^\prime == b$.
        \EndFunction
    \end{algorithmic}
\end{algorithm}
\begin{definition}[Security Game Winning]\label{def:1:2:secwin}
    An adversary wins the security game if $b = b^\prime$.
\end{definition}
\begin{definition}[Advantage of a Distinguisher]\label{def:1:2:advantage}
    Let $\ell$ be a polynomial, $U$ an $\ell$-distinguisher (\ref{def:1:2:ldistinguisher}) and $\mcB$ be an $\ell$-block cipher (\ref{def:1:1:ell-blockcipher}). The \textbf{advantage} of $U$ wrt. $\mcB$ is
    \begin{align*}
        Adv_{U,\mcB}(\eta) = 2 \cdot \left(\Pr[\bbE_U^\mcB(1^\eta) = 1] - \dfrac{1}{2}\right) \in [-1,1]
    \end{align*}
\end{definition}
\begin{definition}[Negligible Function]\label{def:1:2:negligible}
    Let $f: \bbN \to \bbR_{\geq 0}$ be a function. $f$ is \textbf{negligible} if $\forall p$ positive polynomial $\exists N \in \bbN\ \forall n > N: f(n) < \frac{1}{p(n)}$.
\end{definition}
\begin{definition}[Overwhelming Function]\label{def:1:2:overwhelming}
    Let $f: \bbN \to \bbR_{\geq 0}$ be a function, then $f$ is \textbf{overwhelming} if $1 - f$ is negligible (\ref{def:1:2:negligible}).
\end{definition}
\begin{definition}[Security of Block Cipher]\label{def:1:2:secofblock}
    Let $\ell: \bbN \to \bbN$ be a polynomial, $\eta \in \bbN$, $U$ be an $\ell$-distinguisher (\ref{def:1:2:ldistinguisher}) and $\mcB$ an $\ell$-block cipher (\ref{def:1:1:ell-blockcipher}). Then $\mcB$ is secure if $\forall U:\ |Adv_{U,\mcB}|$ is negligible (\ref{def:1:2:negligible}).
\end{definition}
\begin{definition}[Success and Failure of a Distinguisher]\label{def:1:2:sucfail}
    Let $\ell$ be polynomial, $\eta \in \bbN$, $U$ an $\ell$-distinguisher (\ref{def:1:2:ldistinguisher}), and $\mcB$ an $\ell$-block cipher (\ref{def:1:1:ell-blockcipher}). Then the \textbf{success} is $suc_{U,\mcB} = \Pr[\bbS^\mcB_U\langle b = 1 \rangle(1^\eta) = 1]$ and the \textbf{failure} is $fail_{U,\mcB}(\eta) = \Pr[\bbS^\mcB_{U}\langle b = 0 \rangle(1^\eta) = 1]$.
\end{definition}
\begin{lemma}[SucFail is Adv]\label{lem:1:2:sucfailadv}
    Let $\ell$ be a polynomial, $\eta\in\bbN$,$U$ be an $\ell$-distinguisher (\ref{def:1:2:ldistinguisher}) and $\mcB$ an $\ell$-block cipher (\ref{def:1:1:ell-blockcipher}). Then $Adv_{U,\mcB}(\eta) = suc_{U,\mcB}(\eta) - fail_{U,\mcB}(\eta)$
\end{lemma}
\begin{proof}
    \begin{align*}
        \Pr[\bbE(1^\eta) = 1] &= \Pr[\bbS(1^\eta) = b] = \Pr[\bbS(1^\eta) = b, b = 1] + \Pr[\bbS(1^\eta) = b, b = 0] \\
        &= \Pr[\bbS(1^\eta) = 1 | b = 1] \cdot \underbrace{\Pr[b = 1]}_{=\sfrac12} + \Pr[\bbS(1^\eta) = 0 | b = 0] \cdot \underbrace{\Pr[b = 0]}_{=\sfrac12} \\
        &= \Pr[\bbS\langle b = 1 \rangle(1^\eta) = 1] \cdot \tfrac12 + (1 - \Pr[\bbS\langle b = 0 \rangle(1^\eta) = 1]) \cdot \tfrac12 \\
        &= suc_{U,\mcB}(\eta) \cdot \tfrac12 + (1 - fail_{U,\mcB}(\eta))\cdot \tfrac12 \\
        &\implies 2 \left(\Pr[\bbE(1^\eta) = 1] - \frac12\right) = suc_{U,\mcB}(\eta) - fail_{U,\mcB}(\eta)
    \end{align*}
\end{proof}
\begin{lemma}[Permutation vs. Bits in $\bbE$]\label{lem:1:2:permbits}
    Let $U$ be an $\ell$-distinguisher (\ref{def:1:2:ldistinguisher}) with runtime bound $q(\eta)$. Then in $\bbE\langle b = 0 \rangle$, instead of $F \fgets \mcP_{\set{0,1}^{\ell(\eta)}}$, one can instead write a sequence of $y_i \fgets \set{0,1}^{\ell(\eta)} \setminus \set{y_0,...,y_{i-1}}$ for $0 \leq i \leq q(\eta) - 1$ instead, which does not change $U$'s advantage. Then in the Guess Phase, we call $b^\prime \fgets U(1^\eta, (y_0,...,y_{q(\eta) - 1}))$, which means every time $U$ makes a request to the oracle, the next $y_i$ is chosen.
\end{lemma}
\begin{theorem}[Vernam as Block Cipher]\label{thm:1:2:vernamblock}
    Let $\mcB = (M, \Gen(1^\eta), M, E, D)$, $M = \set{0,1}^{\ell(\eta)}$, and $E(x,k) = x \oplus k$ and $D(y,k) = y \oplus k$, and $\Gen(1^\eta)$ returns $k \in \set{0,1}^{\ell(\eta)}$ uniformly at random. Then $\mcB$ is \textit{not} secure.
\end{theorem}
\begin{proof}
    Define $U$ as
    \begin{algorithmic}
        \Function{$U$}{$1^\eta, F$}
            \State $k = F(0^{\ell(\eta)})$ \Comment{In case $F$ is Vernam Cipher, $F(0^{\ell(\eta)})$ is the key}
            \State $y = F(1^{\ell(\eta)})$
            \State \Return $1^\ell \oplus k == y$
        \EndFunction
    \end{algorithmic}
    Then $Adv_{U,\mcB}(\eta) = 1 - \frac{1}{2^{\ell(\eta)} - 1}$: First, it holds true that $suc_{U,\mcB} = \Pr[\bbS\langle b = 1\rangle(1^\eta) = 1] = 1$. For $fail$, we can incoorporate \Cref{lem:1:2:permbits} and replace $F$ with $y_0,y_1$. Then
    \begin{align*}
        fail_{U,\mcB}(\eta) &= \Pr[\bbS\langle b = 0 \rangle(1^\eta) = 1] = \Pr[y_0 = y_1 \oplus 1^{\ell(\eta)}] \\
        &= \sum_{x \in \set{0,1}^{\ell(\eta)}} \Pr\left[y_0 = y_1 \oplus 1^{\ell(\eta)}, y_0 = x\right] \\
        &= \sum_x \Pr\left[y_1 = x \oplus 1^{\ell(\eta)} \middle| y_0 = x\right] \cdot \Pr[y_0 = x] \\
        &= \sum_x \dfrac{1}{2^{\ell(\eta)} - 1} \cdot \dfrac{1}{2^{\ell(\eta)}} = \dfrac{1}{2^{\ell(\eta)} - 1}
    \end{align*}
    Thus, $Adv_{U,\mcB}$ is non-negligible.
\end{proof}
\begin{definition}[Set of Functions]\label{def:1:2:setoffunctions}
    Define $\mcF_{\set{0,1}^{\ell(\eta)}} := \dset{f: \set{0,1}^{\ell(\eta)} \to \set{0,1}^{\ell(\eta)}}{f \text{ is a function}}$.
\end{definition}
\begin{lemma}[PRF/PRP Switching Lemma]\label{lem:1:2:prfprpswitching}
    Let $\mcB$ be an $\ell$-block cipher (\ref{def:1:1:ell-blockcipher}) and $U$ be an $\ell$-distinguisher (\ref{def:1:2:ldistinguisher}) with runtime bound $q(\eta)$ where $q$ is a positive polynomial. Then
    \begin{align*}
        \left|Adv_{U,\mcB}^{PRP}(\eta) - Adv_{U,\mcB}^{PRF}(\eta)\right| \leq \dfrac{q^2(\eta)}{2^{\ell(\eta)} + 1}
    \end{align*}
    and thus negligible.
\end{lemma}
\begin{proof}[Idea]
    First, note that
    \begin{align*}
        |Adv_{U,\mcB}^{PRP}(\eta) - Adv_{U,\mcB}^{PRF}(\eta)| &= |suc^{PRP}(\eta) - fail^{PRP}(\eta) - (suc^{PRF}(\eta) - fail^{PRF}(\eta)) \\
        &= |fail^{PRP}(\eta) - fail^{PRF}(\eta)|
    \end{align*}
    The proof is a sequence of games:
    \begin{enumerate}
        \item Make a rewrite-step for 
    \end{enumerate}
\end{proof}
\begin{auxl}[Difference Lemma]\label{alem:1:2:difference}
    Let $A, B$ be two events defined on the same probability space and $E$ an error event, such that $\Pr[A \cap \overline{E}] = \Pr[B \cap \overline{E}]$. Then $|\Pr[A] - \Pr[B]| \leq \Pr[E]$.
\end{auxl}

\section{Symmetric Encryption Scheme}
\begin{definition}[Symmetric Encryption Scheme]\label{def:1:3:symencscheme}
    A symmetric encryption scheme $\mcS = (\Gen(1^\eta), E, D)$ with security parameter $\eta\in\bbN$
    \begin{itemize}
        \item $\Gen: \set{1^\eta} \to K \subseteq \set{0,1}^*$ is a \textbf{ppt key generation alg.}
        \item $E: \set{0,1}^* \times K \to \set{0,1}^*$ is a \textbf{ppt encryption alg.}
        \item $D: \set{0,1}^* \times K \to \set{0,1}^*$ is a \textbf{poly-time \underline{deterministic} decryption algorithm}
        \item Requirement: $\forall x \in \set{0,1}^*, k \in K, \alpha \in \set{0,1}^{p(|x| + |k|)}:\ D(E^\alpha(x,k), k) = x$
    \end{itemize}
\end{definition}
\begin{definition}[Number Generator]\label{def:1:3:numbergen}
    Let $\eta \in \bbN$, $p$ be a polynomial, and $G: \set{0,1}^\eta \to \set{0,1}^{p(\eta)}$ be a deterministic poly-time algorithm. If $p(\eta) > \eta$, then $G$ is a \textbf{Number Generator} and $p$ is called the \textbf{expansion factor}.
\end{definition}
\begin{definition}[PRNG-Distinguisher]\label{def:1:3:prngdist}
    Let $\eta\in\bbN$ and $p$ be a polynomial. A \textbf{pseudo random number generator distinguisher} $U: \set{1}^\eta \times \set{0,1}^{p(\eta)} \to \set{0,1}$ is a ppt algorithm.
\end{definition}
\begin{definition}[Security Game $\bbE^{PRNG}_{U,G}$]\label{def:1:3:prngsecgame}
    Let $\eta\in\bbN$, $U$ be a PRNG-dist (\ref{def:1:3:prngdist}) and $G$ be a number generator (\ref{def:1:3:numbergen}). The associated game is
    \begin{algorithmic}[1]
        \Function{$\bbE^{PRNG}_{U,G}$}{$1^\eta$}
            \State $b \fgets \set{0,1}$
            \If{$b = 1$}
                \State $s \fgets \set{0,1}^\eta$
                \State $x = G(s)$
            \Else
                \State $x \fgets \set{0,1}^{p(\eta)}$
            \EndIf
            \State $b^\prime \fgets U(1^\eta, x)$
            \State \Return $b == b^\prime$
        \EndFunction
    \end{algorithmic}
\end{definition}
\begin{definition}[Pseudo-Random Number Generator]\label{def:1:3:prng}
    Let $G$ be a number generator (\ref{def:1:3:numbergen}). We call it a \textbf{pseudorandom number generator} if for any PRNG-distinguisher $U$ (\ref{def:1:3:prngdist}), $|Adv_{U,G}(\eta)$ for the security game $\bbE$ (\ref{def:1:3:prngsecgame}) is a negligible function.
\end{definition}