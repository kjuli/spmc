\chapter{Security and Privacy}
\section{Dolev-Yao}
\begin{definition}[DY: Terms and Messages]\label{def:2:1:TM}
    Let
    \begin{itemize}
        \item $\mcA$ finite set of constants: \textbf{atomic messages}.
        \item $\mcK_a \subseteq \mcA$ finite set of public/private keys, where $\cdot^{-1}$ be a bijective function on $\mcK_a$
        \item $\mcV$ set of variables with $\mcV \cap \mcA = \emptyset$
        \item $\Sigma_\mcA = \set{\se{\cdot},\as\cdot,\con{\cdot,\cdot}} \cup \mcA$ be a \textbf{signature}
    \end{itemize}
    The set $\mcT$ of \textbf{Terms} with $T \subseteq \Sigma_\mcA \cup \mcV$ defined as follows:
    \begin{enumerate}
        \item $a \in \mcA \implies a \in \mcT$
        \item $x \in \mcV \implies x \in \mcT$
        \item $t,t^\prime \in \mcT \implies \se{t}_{t^\prime} \in \mcT$
        \item $k \in \mcK_a\ \wedge\ t \in \mcT \implies \as{t}_k \in \mcT$
        \item $t,t^\prime\in\mcT \implies \con{t,t^\prime} \in \mcT$.
    \end{enumerate}
    Write $\mcV(t) := \dset{v \in \mcV}{v \in t}$. If $t \in \mcT$ has no variables, we call $t$ a \textbf{ground term} or a \textbf{message}. Thus, $\mcM = \dset{t \in \mcT}{t \text{ message}}$. 
\end{definition}
\begin{definition}[Substitution]\label{def:2:1:substitution}
    A \textbf{substitution} $\sigma: \mcV \to \mcT$ such that for finite $x$, $\sigma(x) \neq x$. The \textbf{domain} is $\dom(\sigma) = \dset{x \in \mcV}{\sigma(x) \neq x}$. We call $\sigma$ a \textbf{ground substitution} if $\forall x \in \dom(\sigma): \sigma(x) \in M$. We say $m \in \mcM$ matches with $t \in \mcT$ if $\exists \sigma$ ground sub.$: \sigma(t) = m$, and call $\sigma$ a \textbf{matcher} of $m$ and $t$.
\end{definition}
\begin{definition}[Protocol Step]\label{def:2:1:protocolstep}
    A protocol step is a rewrite rule $(t,t^\prime)$ with $t,t^\prime \in \mcT$ and we write $t \to t^\prime$. 
\end{definition}
\begin{definition}[Instance]\label{def:2:1:instance}
    An instance $\Pi$ is a finite sequence $t_0 \to t_0^\prime,...,t_{n-1} \to t_{n-1}^\prime$ of protocol steps such that $\mcV(t_i^\prime) \subseteq \bigcup_{j \leq i} \mcV(t_j)$. We assume that different instances have disjoint sets of variables.
\end{definition}
\begin{definition}[Protocol]\label{def:2:1:protocol}
    A \textit{protocol} $P$ is a tuple $(\set{\Pi_i}_{i < n}, \mcW)$ where $\set{\Pi_i}$ is a family of $n$ instances and $\mcW \subseteq \mcM$ a finite set of messages, the \textbf{initial intruder knowledge}, assuming that $I \in \mcW$.
\end{definition}
\begin{definition}[Derivation]\label{def:2:1:derivation}
    The (infinite) set of messages $d(\mcW) \subseteq \mcM$ is the minimal set $S$ of messages the intruder can derive from $\mcW$: It is $\mcW \subseteq S$ and
    \begin{description}
        \item[Decomposition] $\con{m,m^\prime} \in S \implies m,m^\prime \in S$
        \item[Composition] $m,m^\prime \in S \implies \con{m,m^\prime} \in S$
        \item[Symmetric decryption] $m, \se{m^\prime}_m \in S \implies m^\prime \in S$
        \item[Symmetric encryption] $m, m^\prime \in S \implies \se{m^\prime}_m \in S$
        \item[Asymmetric decryption] $k^{-1} \in \mcK_a \cap S, \as{m}_k \in S \implies m \in S$
        \item[Asymmetric encryption] $k \in \mcK_a \cap S, m \in S \implies \as{m}_k \in S$   
    \end{description}
\end{definition}
\begin{definition}[Execution Ordering]\label{def:2:1:execord}
    Let $P$ be a protocol (\ref{def:2:1:protocol}) with $\Pi_i = r_1^i \to s_1^i,...,r_{n_i}^i \to s_{n_i}^i$. A \textbf{execution ordering} $\pi = (\pi_0,...,\pi_{p-1})$ for some $p \geq 0$ is a sequence such that $\pi_j < n$ for every $j < p$ and $|\pi|_i := |\dset{j < p}{\pi_j = i}| \leq n_i$.
\end{definition}
\begin{definition}[Attack]\label{def:2:1:attack}
    An attack on a protocl $P$ (\ref{def:2:1:protocol}) is a tuple $(\pi,\sigma)$ where $\pi$ is an execution ordering (\ref{def:2:1:execord}) for $P$ and $\sigma$ is a ground substitution (\ref{def:2:1:substitution}) of the variables occuring in $\dset{r_j^i \to s_j^i}{i < n, j \leq n_i: |\pi|_i \geq j}$ such that with $r_l = r_j^i$ and $s_l = s_j^i$ where $i := \pi_{l-1}$ and $j := |(\pi_0,...,\pi_{l-1})|_i$ for every $1 \leq l \leq |\pi|$ it holds true that $\sigma(r_l) \in d(\mcW \cup \set{\sigma(s_1),...,\sigma(s_{l-1})})$.

    The attack is \textbf{successful} if $\secret \in d(\mcW \cup \set{\sigma(s_1),...,\sigma(s_{|\pi|})})$
\end{definition}
\begin{definition}[$\Insecure$]\label{def:2:1:insecureproblem}
    In a protocol $P$, every instance $\Pi$ may only be used once. The \textbf{$\omega$-extension} $P^\omega$ allows to use protocols infinitely many times. The $\Insecure$ decision problems are
    \begin{align*}
        \Insecure &= \dset{P}{\exists (\pi,\sigma) \text{ successful on } P} \\
        \Insecure^\omega &= \dset{P}{\exists (\pi,\sigma) \text{ successful on } P^\omega}
    \end{align*}
    Thus, $P$ is secure if $P \notin \Insecure$ and $P$ is $\omega$-secure if $P \notin \Insecure^\omega$.
\end{definition}
\begin{theorem}[$\Insecure^\omega$ is undecidable]\label{thm:2:1:insecureomundecidable}
    $\Insecure^\omega$ (\ref{def:2:1:insecureproblem}) is undecidable.
\end{theorem}
\begin{proof}[Idea]
    Given $S = (u_1,v_1),...,(u_n,v_n)$ an instance of PCP, we construct a protocol $P_S$ such that $S$ has a solution iff $P_S \in \Insecure^\omega$. Since PCP is undecidable, $\Insecure^\omega$ is undecidable.

    Intruder guesses a reverse sequence of indexes and then the instances of $P_S$ are used to check whether the sequence is a solution of the PCP, with $\mcA = \Sigma \cup \set{1,...,n} \cup \set{k, -, \secret}$. Given $t \in \mcT$ and $w \in \Sigma^*$ we define $w \cdot t$ inductively with $\varepsilon \cdot t = t$ and if $w = va$ for $v \in \Sigma^*$ and $a \in \Sigma$ then $w \cdot t = v \cdot \con{a,t}$. Then the instances of $P_S$ are
    \begin{align*}
        \Pi_1&: \con{x,y} \to \se{\con{\con{x,y},\con{-,-}}}_k \\
        \Pi_2^j&: \se{\con{\con{j,x^\prime},\con{y_l^\prime,y_r^\prime}}}_k \to \se{\con{x^\prime, \con{u_j \cdot y_l^\prime, v_j \cdot y_r^\prime}}}_k \\
        \Pi_3&: \se{\con{-,\con{x^{\prime\prime}, x^{\prime\prime}}}}_k \to \secret
    \end{align*}
    With the initial intruder knowledge $\mcW = \mcA \setminus \set{\secret, k}$.
\end{proof}
\begin{definition}[Derivation as Decisition Probl.]\label{def:2:1:derivdp}
    The derivation problem is
    \begin{align*}
        \Derive = \dset{(E,m)}{E \subseteq \mcM \text{(finite)}, m \in \mcM, m \in d(E)}
    \end{align*}
\end{definition}
\begin{definition}[Subterms]\label{def:2:1:subterms}
    The set of subterms for a term $t \in \mcT$ is
    \begin{enumerate}
        \item $t = f(t_1,...,t_n) \implies \Sub(t) = \set{t} \cup \bigcup_i \Sub(t_i)$
        \item And $for T \subseteq \mcT \implies \Sub(T) = \bigcup_{t \in T} \Sub(t)$
    \end{enumerate}
    The size $|E|_{dag}$ of a set $E$ of messages to be $|\Sub(E)|$.
\end{definition}
\begin{theorem}[$\Derive$ is $\P$]\label{thm:2:1:deriveisp}
    $\Derive\in\P$
\end{theorem}
\begin{proof}[Special Case, symmetric $k \in \mcA$ constant]
    Look at the following algorithm
    \begin{algorithmic}
        \Function{DP}{$E \subseteq \mcM$, $m \in \mcM$}
            \State $E^\prime := E$
            \Repeat \Comment{Derive all possible subterms from $E$}
                \ForAll{$t \in E^\prime$}
                    \State If $t = \con{t^\prime,t^\pprime}$, then $E^\prime := E^\prime \cup \set{t^\prime,t^\pprime}$.
                    \State If $t = \se{t^\prime}_k$ for $k \in E^\prime$, then $E^\prime := E^\prime \cup \set{t^\prime}$.
                    \State If $t = \as{t^\prime}_k$ for $k^\inv \in E^\prime$, then $E^\prime := E^\prime \cup \set{t^\prime}$.
                \EndFor
            \Until{no new (sub-)terms are added to $E^\prime$}
            \State $S = \set{m}$
            \Repeat \Comment{Check whether $m$ can be built from terms in $E^\prime$}
                \ForAll{$t \in S$}
                    \If{$t \in E^\prime$}
                        \State $S := S \setminus \set{t}$
                    \Else
                        \State If $t = \con{t^\prime,t^\pprime}$, then $S := (S \setminus \set{t}) \cup \set{t^\prime,t^\pprime}$
                        \State If $t = \se{t^\prime}_k$ for $k \in E^\prime$, then $S := (S \setminus \set{t}) \cup \set{t^\prime}$
                        \State If $t = \as{t^\prime}_k$ for $k \in E^\prime$, then $S := (S \setminus \set{t}) \cup \set{t^\prime}$
                    \EndIf
                \EndFor
            \Until{$S$ is not changed}
            \State\Return $S == \emptyset$
        \EndFunction
    \end{algorithmic}
    The above algorithm runs in polynomial time in $|E \cup \set{m}|_{dag}$.
\end{proof}
\begin{theorem}[$\Insecure$ is $\NP$-complete]\label{thm:2:1:insecurenp}
    $\Insecure$ is $\NP$-complete.
\end{theorem}
\begin{proof}
    First, note that for a protocol $P$ (\ref{def:2:1:protocol}) with $\Pi_i = (r_j^i \to s_j^i)_{j \leq n_i}$ for $i < n$. Then
    \begin{enumerate}
        \item $\Sub(\Pi_i) = \Sub(\set{r_1^i,...,r_{n_i}^i,s_1^i,...,s_{n_i}^i})$
        \item $\Sub(P) = \bigcup_{i<n} \Sub(\Pi_i) \cup \Sub(\mcW)$
        \item $|P|_{dag} = \max\set{|\Sub(P)|, \sum_i n_i}$
    \end{enumerate}
    Then the NP-decision algorithm is
    \begin{enumerate}
        \item Guess execution ordering $\pi$ of $P$: $\pi = (r_i \to s_i)_{i < p}$ were $r_p = \secret$.
        \item Guess $\sigma$ such that $|\sigma(x)|_{dag} \leq |P|_{dag}$ for every $x \in \mcV(P)$
        \item Check that $\sigma(r_l) \in d(\mcW \cup \dset{\sigma(s_i)}{i < l})$ for every $l \leq p$
        \item Output ``yes'' if all checks are successful.
    \end{enumerate}
    Because of \Cref{thm:2:1:deriveisp}, this algorithm runs in non-deterministic poly-time. Because of \Cref{lem:2:1:sucDAGattack}, the above algorithm is complete and sound and thus $\Insecure \in \NP$.

    Now, consider a SAT formula $\varphi$ in 3CNF, i.e., $\varphi = \bigwedge_i c_i$ where $c_i = l_1 \vee l_2 \vee l_3$ with $l_j = v$ or $l_j = \neg v$ for some variable $v$. Then there is a protocol $P_\varphi$ that outputs the $\secret$ iff $\varphi$ is satisfiable.
\end{proof}
\begin{definition}[$\theta$-match]\label{def:2:1:thetamatch}
    Let $t \in \mcT$, $m \in \mcM$, $\theta$ a ground substitution (\ref{def:2:1:substitution}). Then $t$ is a $\theta$-match of $m$ (written $t\sqsubseteq_\theta m$) if $t \notin \mcV$ and $\theta(t) = m$.
\end{definition}
\begin{lemma}[Successful attack with $\theta$]\label{lem:2:1:thetamatchsuc}
    $\exists (\pi,\sigma)$ successful on $P \implies \exists (\pi,\sigma)$ successful $\forall x \in \dom(\sigma)\ \exists t \in \Sub(P): t \sqsubseteq_\theta \sigma(x)$.
\end{lemma}
\begin{lemma}[Successful DAG attack]\label{lem:2:1:sucDAGattack}
    $\exists (\pi,\sigma)$ successful on $P \implies \exists (\pi,\sigma)$ successful on $|\sigma(x)|_{dag} \leq |P|_{dag}$.
\end{lemma}

\section{Zero-Knowledge Proofs}
\begin{definition}[Iteractive Turing Machines (ITMs)]\label{def:2:2:itm}
    Let $P$ and $V$ be TMs that share a tape and halt after finitely many steps for any input $x$. Let $l_1$ and $l_2$ be upper bounds in $|x|$ for $P$ and $V$. Let $\Omega_1 = \set{0,1}^{l_1(|x|)}$ and $\Omega_2 = \set{0,1}^{l_2(|x|)}$. Then $\con{P,V}(x)$ is a random variable over $\Omega_1 \times \Omega_2$ uniformly distributed. $\con{P,V}(x)(\alpha_1,\alpha_2)$ denotes the local output of $V$ when interacting with $P$ on joint input $x$, where $P$ uses random coins $\alpha_1$ and $V$ uses $\alpha_2$. Furthermore
    \begin{align*}
        \Pr[\con{P,V}(x) = y] := \Pr[\dset{(\alpha_1,\alpha_2) \in \Omega_1 \times \Omega_2}{\con{P,V}(x)(\alpha_1,\alpha_2) = y}]
    \end{align*}
\end{definition}
\begin{definition}[Interactive Proof System (IPS)]\label{def:2:2:ips}
    The ITM pair $(P,V)$ (\ref{def:2:2:itm}) is an IPS for a language $L \subseteq \set{0,1}^*$ if $P$ has bounded runtime (there exists a function $f(|x|)$ that upper bounds $P$, no matter how), $V$ has polynomial runtime and
    \begin{description}
        \item[Completeness] $\forall x \in L:\ \Pr[\con{P,V}(x) = 1] \geq \tfrac23$ (completeness bound)
        \item[Soundness] $\forall x \notin L:\ \forall (P^*,V)$ ITM (\ref{def:2:2:itm}) $:\ \Pr[\con{P^*,V}(x) = 1] \leq \tfrac13$ (soundness bound)  
    \end{description}
\end{definition}
\begin{definition}[Computationally Indistinguishable]\label{def:2:2:compind}
    Let $L$ be a language and $\set{D_x}_{x \in L}$, $\set{D^\prime_x}_{x\in L}$ be families of RVs. Then they are \textbf{Computationally Indistinguishable} if $\forall U$ ppt TM, $\exists f$ negligible (\ref{def:1:2:negligible})$\ \forall x \in L:\ \left|\Pr[U(D_x,x) = 1] - \Pr[U(D_x^\prime,x) = 1]\right| \leq f(|x|)$. We write $\set{D_x}_x \approx_c \set{D_x^\prime}$.
\end{definition}
\begin{definition}[Computational Zero Knowledge]\label{def:2:2:compzk}
    Let $(P,V)$ be IPS (\ref{def:2:2:ips}) for $L \subseteq \set{0,1}^*$. Then $P$ is called computational ZK if $\forall V^*$ ppt ITM connected to $P \exists M^*$ ppt TM$:\ \set{\con{P,V^*}(x)}_{x \in L} \approx_c \set{M^*(x)}_{x \in L}$. Here, $M^*$ is called a \textbf{simulator}.
\end{definition}
\begin{definition}[View of IPS]\label{def:2:2:viewofips}
    Let $(P,V)$ be IPS (\ref{def:2:2:ips}), $V^*$ be as in \Cref{def:2:2:compzk} and $p$ be a polynomial bounding $V^*$. Let $view_{V^*}^P(x)$ be the RV that outputs the random bit string of $V^*$ (the first $p(|x|)$) and all messages that $V^*$ received during the interaction with $P$ on input $x$.
\end{definition}
\begin{definition}[Perfect Zero-Knowledge]\label{def:2:2:perfectzk}
    Let $(P,V)$ be an IPS for a language $L \subseteq \set{0,1}^*$. Then $P$ is called \textbf{perfect zero-knowledge} if $\forall V^*$ ppt ITM to $P\ \exists M^*$ ppt TM $\ \forall x \in L:$
    \begin{enumerate}
        \item $\Pr[M^*(x) = \bot] \leq \tfrac12$
        \item $\con{P,V^*}(x) = m^*(x)$
    \end{enumerate}
    where $m^*(x)$ is a RV that describes the output of $M^*(x)$ under the condition that $M^*(x) \neq \bot$, that is
    \begin{align*}
        \forall y \in \set{0,1}^*:\ \Pr[m^*(x) = y] = \Pr[M^*(x) = y | M^*(x) \neq \bot]
    \end{align*}
\end{definition}
\begin{lemma}[Perfect ZK is Computational ZK]\label{lem:2:2:perfectzkiscompzk}
    Let $(P,V)$ be an IPS (\ref{def:2:2:ips}) for a language $L \subseteq \set{0,1}^*$ and $P$ be perfect ZK (\ref{def:2:2:perfectzk}). Then $P$ is computational ZK (\ref{def:2:2:compzk}).
\end{lemma}
\begin{algorithm}[h!]
    \caption{Simulator $\overline{M}^*$}
    \begin{algorithmic}[1]
        \Function{$\overline{M}^*$}{$x$}
            \For{$i = 1,...,|x|$}
                \State $\alpha_i \fgets \set{0,1}^{t(|x|)}$
            \EndFor
            \For{$i = 1,...,|x|$}
                \State $y = M^*_{\alpha_i}(x)$
                \State If $y \neq \bot$ then \Return $y$
            \EndFor
            \State\Return $\bot$
        \EndFunction
    \end{algorithmic}
    \label{alg:2:2:mbar}
\end{algorithm}
\begin{lemma}[Probability of Aborting]\label{lem:2:2:probabort}
    It holds true that $\Pr[\overline{M}^*(x) = \bot]$ is negligible (using \Cref{alg:2:2:mbar}).
\end{lemma}
\begin{proof}
    Let $M_i^*$ be the RV describing the output of $M^*_{\alpha_i}$. Then
    \begin{align*}
        \Pr\left[\overline{M}^*(x) = \bot\right] &= \Pr[M_i^*(x) = \bot, i \in \set{1,...,|x|}] \\
        &= \prod_{i = 1}^{|x|} \Pr[M_i^*(x) = \bot] \\
        &= \Pr[M^*(x) = \bot]^{|x|} \leq 2^{-|x|}
    \end{align*}
\end{proof}
\begin{lemma}[Identical Distribution]\label{lem:2:2:identicaldist}
    It holds true that $m^*$ and $\overline{m}^*$ are distributed identically.
\end{lemma}
\begin{proof}
    Let $p$ be a polynomial which upper bounds the runtime of $M^*$. Then define
    \begin{align*}
        \Omega_{m^*} &= \dset{\alpha}{\alpha\in\set{0,1}^{p(x)}, M_\alpha^*(x) \neq \bot} \\
        \Omega_{\overline{m}^*} &= \dset{(\alpha_1,...,\alpha_{|x|})}{\forall i \leq |x|: \alpha_i \in \set{0,1}^{p(x)} \wedge \exists j \leq |x|: M_{\alpha_j}^*(x) \neq \bot} \\
        \Omega_{m} &= \dset{\alpha}{\alpha\in\set{0,1}^{p(x)}} \\
        \Omega_{\overline{m}} &= \dset{(\alpha_1,...,\alpha_{|x|})}{\forall i \leq |x|: \alpha_i \in \set{0,1}^{p(x)}}
    \end{align*}
    No map each $\alpha\in\Omega_{m^*}$ to elements in $\Omega_{\overline{m}^*}$ which lead to a successful simulation with random bits $\alpha$.
    \begin{align*}
        S_\alpha = \dset{(\alpha_1,...,\alpha_{|x|}) \in \Omega_{\overline{m}^*}}{\exists j \leq |x|: \alpha_j = \alpha, \forall k < j: M_{\alpha_k}^*(x) = \bot}
    \end{align*}
    \noindent
    \underline{1. $\bigcup_\alpha S_\alpha = \Omega_{\overline{m}^*}$}\\
    ``$\subseteq$'': Holds true by definition of $S_\alpha$. \\
    ``$\supseteq$'': Let $\alpha^* = (\alpha_1,...,\alpha_{|x|}) \in \Omega_{\overline{m}^*}$. By definition, $\exists \alpha_j, j \leq |x|:\ M_{\alpha_j}^*(x) \neq \bot\ \wedge\ \forall k < j: M_{\alpha_k}^* = \bot$. Thus, $\alpha^* \in S_{\alpha_j} \subseteq \bigcup_{\alpha\in\Omega_{m^*}} S_\alpha$.\\
    \noindent
    \underline{2. $\forall \alpha,\alpha^\prime\in\Omega_{m^*}: \alpha \neq \alpha^\prime \implies S_\alpha \cap S_{\alpha^\prime} = \emptyset$} \\
    Holds true by definition.\\
    \noindent
    \underline{3. $\forall \alpha\in\Omega_{m^*}: \Pr_{m^*}[\alpha] = \Pr_{\overline{m}^*}[S_\alpha]$} \\
    In other words, we show that $|S_\alpha| = |\Omega_{\overline{m}^*}|/|\Omega_{m^*}|$.
    \begin{align*}
        |S_\alpha| &= \sum_{i = 0}^{|x| - 1} |\overline{\Omega_{m^*}}|^{|x| - 1 - i} \cdot |\set{0,1}^{p(|x|)}|^i \\
        &= \dfrac{|\set{0,1}^{p(|x|)}|^{|x|} - |\overline{\Omega_{m^*}}|^{|x|}}{|\set{0,1}^{p(|x|)}| - |\overline{\Omega_{m^*}}|} \\
        &= \dfrac{|\set{0,1}^{p(|x|)}|^{|x|} - |\overline{\Omega_{m^*}}|^{|x|}}{|\Omega_{m^*}|} \\
        &= \dfrac{|\set{0,1}^{p(|x|)|x|}| - |\overline{\Omega_{\overline{m}^*}}|}{|\Omega_{m^*}|} \\
        &= \dfrac{|\Omega_{\overline{m}^*}|}{|\Omega_{m^*}|}
    \end{align*}
    Note that generally
    \begin{align*}
        \dfrac{a^n - b^n}{a - b} = \sum_{i = 0}^{n - 1} a^i b^{n-1-i}
    \end{align*}
\end{proof}

\begin{auxl}[Conditional Probabilities on Uniform Distribution]\label{auxl:2:2:condprob}
    Let $\Omega$ be a finite non-empty set and $P$ the uniform distribution over $\Omega$. Let $B \subseteq \Omega$. Then $P(\cdot | B)$ is a uniform dist. over $B$.
\end{auxl}
\begin{proof}
    Let $\Omega, P, B$ be defined as above and $A \subseteq B$. Then
    \begin{align*}
        \Pr[A | B] = \dfrac{\Pr[A \wedge B]}{\Pr[B]} = \dfrac{\Pr[A]}{\Pr[B]} = \dfrac{\tfrac{|A|}{|\Omega|}}{\tfrac{|B|}{|\Omega|}} = \dfrac{|A|}{|B|}
    \end{align*}
\end{proof}