\babel@toc {english}{}
\contentsline {section}{\numberline {Homework 0:}Possibilistic Security, Vernam and Probability}{}{section.0}%
\contentsline {subsection}{\numberline {Problem 1:}Possibilistic Security}{}{subsection.0.1}%
\contentsline {subsection}{\numberline {Problem 2:}Variant of Vernam}{}{subsection.0.2}%
\contentsline {subsection}{\numberline {Problem 3:}Properties of probability distributions}{}{subsection.0.3}%
\contentsline {section}{\numberline {Homework 1:}Probability and Perfect Secrecy}{}{section.1}%
\contentsline {subsection}{\numberline {Problem 1:}Conditional Probability is a Probability Distribution}{}{subsection.1.1}%
\contentsline {subsection}{\numberline {Problem 2:}Alternative formulation of independent events}{}{subsection.1.2}%
\contentsline {subsection}{\numberline {Problem 3:}Perfect secrecy of Cryptosystems}{}{subsection.1.3}%
\contentsline {subsection}{\numberline {Problem 4:}Probabilities of Plaintexts in Cryptosystems}{}{subsection.1.4}%
\contentsline {subsection}{\numberline {Problem 5:}Perfect secrecy and key distributions}{}{subsection.1.5}%
\contentsline {section}{\numberline {Homework 2:}SPCS, Linear dependencies and Probabilistic Algorithms}{}{section.2}%
\contentsline {subsection}{\numberline {Problem 1:}Importance of the $S$-Box in SPCS}{}{subsection.2.1}%
